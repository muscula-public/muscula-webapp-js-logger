function attachClickEvent(o, fn) {
	if (o.addEventListener) { // DOM standard
		o.addEventListener('click', fn, false);
	} else if (o.attachEvent) { // IE
		o.attachEvent('onclick', fn);
	}
}
function detachClickEvent(o, fn) {
	if (o.removeEventListener) { // DOM standard
		o.removeEventListener('click', fn, false);
	} else if (o.detachEvent) { // IE
		o.detachEvent('onclick', fn);
	}
}
function throwError() {
	var i = null;var j = i.does_not_exist;
}
var o = document.getElementById('test1');
attachClickEvent(o, function (e) { throwError() });

o = document.getElementById('ajax');
attachClickEvent(o, function (e) {
	alert("kalder ajax");
	$.get('index.htm', function (data) {
		//$('.result').html(data);
		//alert('Load was performed.');
		//alert("ajax: success")
		throwError();
	});
});

o = document.createElement("div");
o.innerHTML = " Click me too, I'm created dynamically and I have a click event, that fails. Attached with addEventListener";
attachClickEvent(o, function () { var i = null; var j = i.does_not_exist });
document.getElementById("eventlogtest").appendChild(o);

o = document.createElement("div");
o.innerHTML = " Click me too, I'm created dynamically and I have a click event, that fails. Attached with onclick property";
o.onclick = function () { var i = null; var j = i.does_not_exist };
document.getElementById("eventlogtest").appendChild(o);

var o = document.getElementById('settimeout');
attachClickEvent(o, function () {
	setTimeout(function (){
		setTimeout(function () { var i = null; var j = i.does_not_exist }, 1000);
	}, 10);
});

function onloadTest() {
	var o = document.getElementById('test2');
	var fn = function () { detachClickEvent(document.getElementById('test2'), fn); /*var i = null; var j = i.does_not_exist;*/alert("hej") };
	attachClickEvent(o, fn);

	o = document.createElement("div");
	o.innerHTML = " Click me too, I'm created dynamically and I have a click event, that fails. Attached with addEventListener";
	attachClickEvent(o, function () { var i = null; var j = i.does_not_exist });
	document.getElementById("eventlogtest2").appendChild(o);

	o = document.createElement("div");
	o.innerHTML = " Click me too, I'm created dynamically and I have a click event, that fails. Attached with onclick property";
	o.onclick = function () { var i = null; var j = i.does_not_exist };
	document.getElementById("eventlogtest2").appendChild(o);

	var o = document.getElementById('settimeout2');
	attachClickEvent(o, function () {
		setTimeout(function () { var i = null; var j = i.does_not_exist }, 1000);
	});
	var o = document.getElementById('test3');
	attachClickEvent(o, function () { var i = null; var j = i.does_not_exist });

	o = document.createElement("div");
	o.innerHTML = " Click me too, I'm created dynamically and I have a click event, that fails. Attached with addEventListener";
	attachClickEvent(o, function () { var i = null; var j = i.does_not_exist });
	document.getElementById("eventlogtest3").appendChild(o);

	o = document.createElement("div");
	o.innerHTML = " Click me too, I'm created dynamically and I have a click event, that fails. Attached with onclick property";
	o.onclick = function () { var i = null; var j = i.does_not_exist };
	document.getElementById("eventlogtest3").appendChild(o);

	var o = document.getElementById('settimeout3');
	attachClickEvent(o, function () {
		setTimeout(function () { var i = null; var j = i.does_not_exist }, 1000);
	});
}

if (window.addEventListener) {
	window.addEventListener("load", onloadTest, false);
} else if (window.attachEvent) {
	window.attachEvent("onload", onloadTest);
} else if (window.onLoad) {
	window.onload = onloadTest;
}