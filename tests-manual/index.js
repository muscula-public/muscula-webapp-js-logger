var express = require("express");

var app = express();
app.configure(function () {
    app.use(express.logger());
    app.use(express.compress());
    app.use(app.router);
    app.use(express.static(__dirname));
});

app.get("/", function (req, res, next) {
    req.url = "/indexUdv.htm";
    next();
});
app.get("/test", function (req, res, next) {
	req.url = "/index.htm";
	next();
});
app.get("/events", function (req, res, next) {
	req.url = "/removeEvents.htm";
	next();
});
app.get("/tests", function (req, res, next) {
	req.url = "/tests.htm";
	next();
});
app.get("/ol", function (req, res, next) {
	req.url = "/ol.htm";
	next();
});
app.get("/p", function (req, res, next) {
	req.url = "/indexp.htm";
	next();
});

app.listen(7070);

console.log("App listening on http://localhost:7070");

var open = require('open');
open('http://localhost:7070/tests');