module.exports = {
  'Check getting log messages': function(client) {
    client
      .url('http://localhost:8080/tests/tests.html')
      .getLogTypes(function(result) {
        console.log(result);
      })
      .getLog('browser', function(result) {
        console.log(result);
      })
      .waitForElementVisible('#musculaDevelopmentInfo', 2000)
      .expect.element('#musculaDevelopmentInfo')
      .text.to.includes('lineNumber');

    return client;
  },
};
