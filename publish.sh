#!/bin/sh
echo "Publishing in NPM"
#npm publish

echo "Publishing in CDN"
npm run build
cp dist/index.js dist/m2v2-olivier.js
ncftpput -v -u jscdn -p $MUSCULA_BUNNY_JSCDN_API_KEY storage.bunnycdn.com /jscdn/ ./dist/m2v2-olivier.js
ncftpput -v -u jscdn -p $MUSCULA_BUNNY_JSCDN_API_KEY storage.bunnycdn.com /jscdn/ ./dist/index.js.map
