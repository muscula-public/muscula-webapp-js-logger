if (window && !window['Promise']) {
    throw new Error('Not compatible browser detected, Muscula logging will not be started. Please use v1. script for support');
}

import StackTrace from 'stacktrace-js';
import {StackFrame} from 'stacktrace-js';

const Severity = {
    Fatal: 0,
    Error: 1,
    Warning: 2,
    Info: 3,
    Debug: 4,
    Trace: 5,
};

type MusculaWindowConfig = {
    logId: string;
    harvesterUrl?: string;
    onerrorOriginalHandler: any;
    errors: Array<any>;
    info: object;
    skipBind: boolean;
};

declare global {
    interface Window {
        Muscula: MusculaWindowConfig;
        MusculaLog: any;
    }
}

window.Muscula = window.Muscula || {};


let developmentInfo: any;

type MusculaConfig = {
    logId: string;
    harvesterUrl?: string;
}

type MusculaRequest = {
    logId: string;
    severity: number;
    message: string;
    pageUrl?: string;
    jsUrl?: string;
    lineNumber?: number;
    column?: number;
    printStack?: string[];
    userAgent?: string;
    exception?: unknown;
    structuralData?: unknown;
}


const musculaConfig: MusculaConfig = {
    logId: '',
    harvesterUrl: 'https://harvester.muscula.com',
};

export default class MusculaLog {

    static Init(logId: string, harvesterUrl?: string) {
        musculaConfig.logId = logId;
        if (harvesterUrl) {
            musculaConfig.harvesterUrl = harvesterUrl;
        }

        if (typeof window !== 'undefined' && !window.Muscula.skipBind) {
            this.BindOnError();
        }

    }

    static OnError(
        msg: Event | string,
        url: string | undefined,
        linenumber: number | undefined,
        column: number | undefined,
        errorObj: Error | undefined) {

        try {
            //transform errors
            if (typeof msg === 'object' &&
                msg.srcElement &&
                msg.target &&
                // @ts-ignore
                msg.srcElement === '[object HTMLScriptElement]' &&
                // @ts-ignore
                msg.target === '[object HTMLScriptElement]') {
                msg = 'Error loading script';
                // @ts-ignore
            } else if (msg && msg.srcElement) {
                // @ts-ignore
                msg = 'Error in event srcElement: ' + msg.srcElement + ', target:' + msg.target;
            } else if (msg && msg.toString) {
                msg = msg.toString();
            }

            if (errorObj) {
                StackTrace.fromError(errorObj)
                    .then(stackTrace => {
                        MusculaLog.Send(Severity.Error, msg + '', {}, errorObj, stackTrace, url, linenumber, column);
                    })
                    .catch(() => {
                        MusculaLog.Send(Severity.Error, msg + '', {}, errorObj, undefined, url, linenumber, column);
                    });
            }

        } catch (ex) {
            MusculaLog.Error('Muscula Error when handling onError', ex);
        }

        if (typeof window.Muscula.onerrorOriginalHandler === 'function') {
            window.Muscula.onerrorOriginalHandler(msg, url, linenumber, column, errorObj);
        }

        return false;
    }

    static BindOnError() {
        window['onerror'] = MusculaLog.OnError;

        if (Array.isArray(window.Muscula.errors)) {
            for (let e of window.Muscula.errors) {
                // @ts-ignore
                MusculaLog.OnError(...e);
            }
        }
    }

    private static ErrorWithStackTrace(severity: number, message: string, exception: Error, structuralData?: unknown) {
        if (typeof exception === 'object') {
            StackTrace.fromError(exception)
                .then(stackTrace => {
                    this.Send(severity, message, structuralData, exception, stackTrace);
                })
                .catch(() => {
                    this.Send(severity, message, structuralData, exception);
                });
        } else {
            try {
                StackTrace.generateArtificially().then((stackTrace) => {
                    this.Send(severity, message, structuralData, undefined, stackTrace);
                }).catch(() => {
                    this.Send(severity, message, structuralData, undefined, undefined);
                });

            } catch (e) {
                console.error('Error when generating artificially stacktrace');
            }
        }
    }

    static Send(
        severity: number,
        message: string,
        structuralData?: unknown,
        exception?: unknown,
        stacktrace?: StackFrame[],
        jsUrl?: string,
        lineNumber?: number,
        column?: number) {
        try {
            if (!musculaConfig.logId) {
                console.error('Muscula Log Id must be provided');
                return;
            }

            let printStack;
            if (stacktrace) {
                //"C@https://app.muscula.com/static/js/2.3f167a04.chunk.js:2:1007185",
                printStack = stacktrace.map(s => s.functionName + '@' + s.fileName + ':' + s.lineNumber + ':' + s.columnNumber);
                if (!jsUrl || !lineNumber) {
                    const s = stacktrace.pop();
                    if (s) {
                        lineNumber = s.lineNumber;
                        jsUrl = s.fileName;
                        column = s.columnNumber;
                    }

                }

            }

            const req: MusculaRequest = {
                logId: musculaConfig.logId,
                severity,
                message,
                pageUrl: window.document !== undefined ? window.document.location.href : undefined,
                jsUrl,
                lineNumber,
                column,
                printStack,
                userAgent: navigator.userAgent,
                exception: exception,
            };

            if (structuralData != null && Object.keys(structuralData as object)) {
                req.structuralData = structuralData;
            }

            const requestString = JSON.stringify(req);

            if (developmentInfo) {
                console.info(req);
                developmentInfo.innerHTML = requestString;
                return;
            }

            const xhr = new XMLHttpRequest();
            if ('withCredentials' in xhr || navigator.product === 'ReactNative') {
                /* supports cross-domain requests */
                xhr.open('POST', musculaConfig.harvesterUrl + '/js');
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.send(requestString);
                return;
            }

            // Add the iframe with a unique name
            let iframe = document.createElement('iframe');
            let uniqueString = 'UNIQUE_' + new Date().getTime() + Math.random();
            document.body.appendChild(iframe);
            iframe.style.display = 'none';
            if (iframe.contentWindow) {
                iframe.contentWindow.name = uniqueString;
            }
            // construct a form with hidden inputs, targeting the iframe
            let form = document.createElement('form');
            form.target = uniqueString;
            form.action = musculaConfig.harvesterUrl + '/jsform';
            form['method'] = 'POST';
            let input = document.createElement('input');
            input.type = 'hidden';
            input.name = 'message';
            input.value = requestString;
            form.appendChild(input);
            document.body.appendChild(form);
            form.submit();
        } catch (e) {
            console.log('Muscula: Error on send', e);
        }

    }

    static SetDevelopmentMode() {
        if (typeof window === 'undefined') {
            return;
        }
        window.document.body.innerHTML += '<div id="musculaDevelopmentInfo"></div>';
        developmentInfo = window.document.querySelector('#musculaDevelopmentInfo');
    }

    static Fatal(message: string, exception?: unknown, structuralData?: unknown) {
        this.ErrorWithStackTrace(Severity.Fatal, message, exception as Error, structuralData);
    }

    static Error(message: string, exception: unknown, structuralData?: unknown) {
        this.ErrorWithStackTrace(Severity.Error, message, exception as Error, structuralData);
    }

    static Info(message: string, structuralData?: unknown) {
        this.Send(Severity.Info, message, structuralData);
    }

    static Warning(message: string, structuralData?: unknown) {
        this.Send(Severity.Warning, message, structuralData);
    }

    static Debug(message: string, structuralData?: unknown) {
        this.Send(Severity.Debug, message, structuralData);
    }

    static Trace(message: string, structuralData?: unknown) {
        this.Send(Severity.Trace, message, structuralData);
    }
}

if (window.Muscula.logId) {
    window.MusculaLog = MusculaLog;
    window.MusculaLog.Init(window.Muscula.logId, window.Muscula.harvesterUrl);
}

