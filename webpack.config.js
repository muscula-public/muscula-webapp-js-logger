const path = require('path');

module.exports = {
  mode: 'production',
  entry: {
    main: "./src/index.ts",
  },
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: "index.js",
    library: {
      name: 'MusculaLog',
      type: 'umd',
    },
    libraryExport: 'default',
    clean: true
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader"
      }
    ]
  }
};
