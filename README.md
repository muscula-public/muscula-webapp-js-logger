# muscula-webapp-js-logger

## Usage

#### Install npm package
```npm install --save @muscula.com/muscula-webapp-js-logger```

#### Initialize library at app startup:
 
```MusculaLog.Init(<LogId>)```

#### Use it:
```
try {
    throw new Error('Error');
} catch(exception) {
    MusculaLog.Error('Something wrong', exception, {userId: ''}})
}
``` 
    
You can pass your own parameters to info object (third parameter).  

